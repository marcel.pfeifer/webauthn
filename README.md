# About this sample app

This is a simple NodeJS app that demonstrates the Web Authentication APIs.

## Deploying a local instance

1. Download and install [NodeJS 8.9 or newer](https://nodejs.org/en/)
2. Download and install [VS Code](https://code.visualstudio.com/)
3. Clone this repository
4. Open this repository in VS Code
5. Run npm install in the root directory
6. Launch program - configurations should already be set
7. Navigate to localhost:3000

## Do it yourself

In the server branch you need to develop the serverside code for an WebAuthn Client

In the client branch you need to develop the clientside code for an WebAuthn Server

### Based of

[MicrosoftEdge/webauthnsample](https://github.com/MicrosoftEdge/webauthnsample)
